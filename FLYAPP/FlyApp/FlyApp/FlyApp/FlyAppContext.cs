﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace FlyApp
{
    public class FlyAppContext:DbContext
    {
        public FlyAppContext():base("FlyAppData")
        {
            
        }
        static FlyAppContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<FlyAppContext>());
        }
        public DbSet<Aircraft> Aircrafts { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<FlyPartnership> FlyPartnerships { get; set; }
        public DbSet<Pilot> Pilots { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketType> TicketTypes { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
