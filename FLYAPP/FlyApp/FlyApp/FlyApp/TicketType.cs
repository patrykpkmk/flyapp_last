﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace FlyApp
{
    public enum TicketClass {Blank, Economic,Business};
   public class TicketType:Entity
    {
        [Required]
        public TicketClass Class { get; set; }
        [Required]
        public bool IsGroup { get; set; }
        [Required]
        public bool IsStudent { get; set; }
        [Required]
        public double Price { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public TicketType() { }
    }
}
