﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FlyApp
{
    public class Ticket:Entity
    {
     
        public int UserId { get; set; }
        [Required]
        public int SeatNumber { get; set; }
        [Required]
        public int TicketTypeId { get; set; }
        [Required]
        public int FlyPartnershipId { get; set; }
        public Ticket() { }
    }
}
