﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FlyApp
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }
        [Timestamp]
        public byte[] Version { get; set; }
        [Required]
        public bool IsDeleted { get; set; } = false;
    }
}
