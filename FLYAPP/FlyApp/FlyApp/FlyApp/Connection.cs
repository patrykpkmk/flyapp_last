﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyApp
{
    public class Connection : Entity
    {
        [Required]
        public int AircraftId { get; set; }
        [Required]
        public int JourneyDistance { get; set; }
        [Required]
        public DateTime ArrivalTime { get; set; }
        [Required]
        public string ArrivalPlace { get; set; }
        [Required]
        public DateTime DepartureTime { get; set; }
        [Required]
        public string DeparturePlace { get; set; }
        [Required]
        public int FlyPartnershipId { get; set; }
        public Connection() { }
    }
}
