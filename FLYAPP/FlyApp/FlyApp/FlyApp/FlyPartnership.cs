﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FlyApp
{
    public class FlyPartnership:Entity
    {
        [Required]
        public string Name { get; set; }
        public virtual ICollection<Connection> Connections { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<Stock> Stosks { get; set; }

        public FlyPartnership() { }
       
    }
}
