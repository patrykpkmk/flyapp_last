﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FlyApp
{
   public  class Stock:Entity
    {
        public virtual ICollection<Aircraft> Aircrafts{ get; set; }
        public virtual ICollection<Pilot> Pilots { get; set; }
        [Required]
        public int FlypartnershipId { get; set; }
        public Stock() { }
    }
}
