﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace FlyApp
{
    public enum SkillType {Blank, Low, Medium, High }
    public class Aircraft : Entity
    {
        [Required]
        public string Model { get; set; }
        [Required]
        public int NumberOfSeats { get; set; }
        [Required]
        public SkillType SkillsRequired { get; set; }
        [Required]
        public int StockId { get; set; }

        public Aircraft() { }

    }
}
