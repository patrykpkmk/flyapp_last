﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyApp
{
   public class User:Entity
    {
        public virtual ICollection<Ticket> Tickets { get; set; }

        public User() { }
    }
}
