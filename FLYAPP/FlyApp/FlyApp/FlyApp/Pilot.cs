﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyApp
{
   public class Pilot:Entity
    {
        [Required]
        public SkillType Skills { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }

        public Pilot() { }
    }
}
