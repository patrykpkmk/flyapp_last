﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Collections.ObjectModel;
using FlyApp;
using System.Windows.Forms;

namespace FlyAppViewModel
{
    public class Connection : FlyAppBase, IConnection
    {

        private string startPlace;
        private string destinationPlace;
        private DateTime dayOfDeparture = DateTime.Today;
        private DateTime dayOfArrival = DateTime.Today;
        private int hourOfDeparture;
        private int minuteOfDeparture;
        private int hourOfArrival;
        private int minuteOfArrival;
        private ObservableCollection<FlyApp.Connection> list;

        public string StartPlace
        {
            get { return startPlace; }
            set
            {
                startPlace = value;
                NotifyOfPropertyChange(() => StartPlace);
            }
        }

        public string DestinationPlace
        {
            get { return destinationPlace; }
            set
            {
                destinationPlace = value;
                NotifyOfPropertyChange(() => DestinationPlace);
            }
        }


        public DateTime DayOfDeparture
        {
            get { return dayOfDeparture; }
            set
            {
                dayOfDeparture = value;
                NotifyOfPropertyChange(() => DayOfDeparture);
            }
        }

        public DateTime DayOfArrival
        {
            get { return dayOfArrival; }
            set
            {
                dayOfArrival = value;
                NotifyOfPropertyChange(() => DayOfArrival);
            }
        }

        public int HourOfDeparture
        {
            get { return hourOfDeparture; }
            set
            {
                hourOfDeparture = value;
                NotifyOfPropertyChange(() => HourOfDeparture);
            }
        }
        public int MinuteOfDeparture
        {
            get { return minuteOfDeparture; }
            set
            {
                minuteOfDeparture = value;
                NotifyOfPropertyChange(() => MinuteOfDeparture);
            }
        }
        public int HourOfArrival
        {
            get { return hourOfArrival; }
            set
            {
                hourOfArrival = value;
                NotifyOfPropertyChange(() => HourOfArrival);
            }
        }
        public int MinuteOfArrival
        {
            get { return minuteOfArrival; }
            set
            {
                minuteOfArrival = value;
                NotifyOfPropertyChange(() => MinuteOfArrival);
            }
        }

        public ObservableCollection<FlyApp.Connection> ConnectionList
        {
            get { return list; }
            set
            {
                list = value;
                NotifyOfPropertyChange(() => ConnectionList);
            }
        }

        public bool SearchConnectionById(int _id, ref List<FlyApp.Connection> result)
        {

            using (var db = new FlyAppContext())
            {
                var con = from a in db.Connections
                          where a.Id == _id && a.IsDeleted == false
                          select a;

                if (!con.Any())
                {
                    MessageBox.Show("Connection not found.");
                    result = null;
                    return false;
                }
                else
                {
                    List<FlyApp.Connection> air = con.ToList();
                    //MessageBox.Show(air.First().Model.ToString());
                    //AircraftList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Aircraft>(aircrafts.AsEnumerable());
                    result = air;
                    return true;
                }

            }

        }
    }
}
