﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyAppViewModel
{
    interface IConnection
    {
         string StartPlace { get; set; }
        string DestinationPlace { get; set; }
         DateTime DayOfDeparture { get; set; }
         DateTime DayOfArrival { get; set; }
        int HourOfDeparture { get; set; }
         int MinuteOfDeparture { get; set; }
         int HourOfArrival { get; set; }
        int MinuteOfArrival { get; set; }
        
    }
}
