﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Windows.Forms;
namespace FlyAppViewModel
{
   public class FlyAppBase:PropertyChangedBase
    {
        private bool idSection;
        private bool criteriaSection;
        private int id;
        private ListBox searchResult;
        public bool IdSection
        {
            get { return idSection; }
            set
            {
                idSection = value;
                
                NotifyOfPropertyChange(() => IdSection);
            }
        }
        public bool CriteriaSection
        {
            get { return criteriaSection; }
            set
            {
                criteriaSection = value;
             
                NotifyOfPropertyChange(() => CriteriaSection);
            }
        }
        public int ID
        {
            get { return id; }
            set { id = value;
                NotifyOfPropertyChange(() => ID);
            }
        }
        public ListBox SearchResult
        {
            get { return searchResult; }
            set
            {
                
               
                searchResult = value;
                NotifyOfPropertyChange(() => SearchResult);
            }
        }
    }
}
