﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyAppViewModel
{
   public class TicketType:FlyAppBase
    {
        
        private bool isGroup;
        private bool isStudent;
        private float prize;
        // private TicketClass ticketClass;
      

        #region properties
        public bool IsGroup
        {
            get { return isGroup; }
            set
            {
                isGroup = value;

                NotifyOfPropertyChange(() => IsGroup);
            }
        }

        public bool IsStudent
        {
            get { return isStudent; }
            set
            {
                isStudent = value;

                NotifyOfPropertyChange(() => IsStudent);
            }
        }
       
        public float Prize
        {
            get { return prize; }
            set
            {
                prize = value;

                NotifyOfPropertyChange(() => Prize);
            }
        }
        //public TicketClass Class
        //{
        //    get { return ticketClass; }
        //    set
        //    {
        //        ticketClass = value;

        //        NotifyOfPropertyChange(() => Class);
        //    }
        //}
        #endregion
    }
}
