﻿using FlyApp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlyAppViewModel
{
    public class Aircraft:FlyAppBase
    {
      
        private string model;
        private int numberOfSeats;
        private SkillType skills;
        private ObservableCollection<FlyApp.Aircraft> list;


        public string Model
        {
            get { return model; }
            set
            {
                model = value;
                NotifyOfPropertyChange(() => Model);
            }
        }
        public int NumberOfSeats
        {
            get { return numberOfSeats; }
            set
            {
                numberOfSeats = value;
                NotifyOfPropertyChange(() => NumberOfSeats);
            }
        }

        public SkillType Skills
        {
            get { return skills; }
            set
            {
                skills = value;
                NotifyOfPropertyChange(() => Skills);
            }
        }


        public ObservableCollection<FlyApp.Aircraft> AircraftList
        {
            get { return list; }
            set
            {
                list = value;
                NotifyOfPropertyChange(() => AircraftList);
            }
        }

        public bool SearchAircraftById(int _id, ref List<FlyApp.Aircraft> result)
        {
           
                using (var db = new FlyAppContext())
                {
                    var aircrafts = from a in db.Aircrafts
                                    where a.Id == _id && a.IsDeleted == false 
                                    select a;

                    if (!aircrafts.Any())
                    {
                        MessageBox.Show("Aircraft not found.");
                        return false;
                    }
                    else
                    {
                        List<FlyApp.Aircraft> air = aircrafts.ToList();
                    //MessageBox.Show(air.First().Model.ToString());
                    //AircraftList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Aircraft>(aircrafts.AsEnumerable());
                        result = air;
                        return true;
                    }

                }
            }
        
   

    }
}
