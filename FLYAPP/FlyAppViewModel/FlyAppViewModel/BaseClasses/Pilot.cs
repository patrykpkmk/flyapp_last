﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using FlyApp;
using LinqKit;
using FlyAppView;
using System.Windows.Forms;

namespace FlyAppViewModel
{
    public class Pilot:FlyAppBase,IPilot
    {
        private string firstname;
        private string surname;
        private SkillType skills;
        
        public string Firstname
        {
            get { return firstname; }
            set
            {
                firstname = value;
                NotifyOfPropertyChange(() => Firstname);
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                NotifyOfPropertyChange(() => Surname);
            }
        }

        public SkillType Skills
        {
            get { return skills; }
            set
            {
                skills = value;
                NotifyOfPropertyChange(() => Skills);
            }
        }
      

    }
}
