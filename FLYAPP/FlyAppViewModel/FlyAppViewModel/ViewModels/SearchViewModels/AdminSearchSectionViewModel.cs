﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace FlyAppView
{
   public class AdminSearchSectionViewModel : PropertyChangedBase
    {
        private AdminSearchConnectionViewModel adminSearchConnection;
        private AdminSearchPilotViewModel adminSearchPilot;
        private AdminSearchTicketTypeViewModel adminSearchTicketType;
        private AdminSearchAircraftViewModel adminSearchAircraft;
        public AdminSearchConnectionViewModel AdminSearchConnectionView
        {
            get { return adminSearchConnection; }
            set
            {
                adminSearchConnection = value;
                NotifyOfPropertyChange(() => AdminSearchConnectionView);
            }
        }
        public AdminSearchPilotViewModel AdminSearchPilotView
        {
            get { return adminSearchPilot; }
            set
            {
                adminSearchPilot = value;
                NotifyOfPropertyChange(() => AdminSearchPilotView);
            }
        }

        public AdminSearchTicketTypeViewModel AdminSearchTicketTypeView
        {
            get { return adminSearchTicketType; }
            set
            {
                adminSearchTicketType= value;
                NotifyOfPropertyChange(() => AdminSearchTicketTypeView);
            }
        }
        public AdminSearchAircraftViewModel AdminSearchAircraftView
        {
            get { return adminSearchAircraft; }
            set
            {
                adminSearchAircraft = value;
                NotifyOfPropertyChange(() => AdminSearchAircraftView);
            }
        }
        public AdminSearchSectionViewModel()
        {
            AdminSearchConnectionView = new AdminSearchConnectionViewModel();
            AdminSearchPilotView = new AdminSearchPilotViewModel();
            AdminSearchTicketTypeView = new AdminSearchTicketTypeViewModel();
            AdminSearchAircraftView = new AdminSearchAircraftViewModel();
        }
    }
}
