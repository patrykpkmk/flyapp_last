﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using FlyAppViewModel;
using System.Windows.Forms;
using FlyApp;

namespace FlyAppView
{
    public class AdminSearchAircraftViewModel: FlyAppViewModel.Aircraft
    {
       
        public AdminSearchAircraftViewModel() { }
        public void AdminSearchForAircraft()
        {

            if (IdSection == true)
            {
                //int _id = ID;
                //using (var db = new FlyAppContext())
                //{
                //    var aircrafts = from a in db.Aircrafts
                //                    where a.Id == _id && a.IsDeleted == false
                //                    select a;

                //    if (!aircrafts.Any())
                //    {
                //        MessageBox.Show("Aircraft not found.");
                //    }
                //    else
                //    {
                //        List<FlyApp.Aircraft> air = aircrafts.ToList();
                //        //MessageBox.Show(air.First().Model.ToString());
                //        AircraftList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Aircraft>(aircrafts.AsEnumerable());
                //    }

                //}
                List<FlyApp.Aircraft> ar = new List<FlyApp.Aircraft>();
            SearchAircraftById(ID, ref ar);
            AircraftList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Aircraft>(ar.AsEnumerable());

        }
            if (CriteriaSection)
            {
                using (var db = new FlyAppContext())
                {
                    var aircrafts = from b in db.Aircrafts
                                    where b.Model.Contains(Model) && Math.Abs(b.NumberOfSeats - NumberOfSeats) <= 50 && b.SkillsRequired == Skills && b.IsDeleted==false && Skills!=0
                                    select b;


                    if (!aircrafts.Any())
                    {
                        MessageBox.Show("Aircraft not found.");
                    }
                    else
                    {
                        List<FlyApp.Aircraft> FoundedAircraftsList = aircrafts.ToList();

                        AircraftList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Aircraft>(aircrafts.AsEnumerable());

                    }
                }
            }

        }
    }
    
}
