﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Windows.Forms;
using FlyAppViewModel;
using FlyApp;

namespace FlyAppView
{
    public class AdminSearchConnectionViewModel : FlyAppViewModel.Connection
    {
        
        public AdminSearchConnectionViewModel()
        {

        }
     
        public void AdminSearchForConnection()
        {
            
            if(IdSection)
            {
                //int _id = ID;
                //using (var db = new FlyAppContext())
                //{
                //    var con = from a in db.Connections
                //                    where a.Id == _id && a.IsDeleted==false
                //                    select a;

                //    if (!con.Any())
                //    {
                //        MessageBox.Show("Connection not found.");
                //    }
                //    else
                //    {
                //        List<FlyApp.Connection> conlist = con.ToList();
                //        //MessageBox.Show(air.First().Model.ToString());
                //        ConnectionList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Connection>(con.AsEnumerable());
                //    }

                //}

                List<FlyApp.Connection> ar = new List<FlyApp.Connection>();
                SearchConnectionById(ID, ref ar);
                ConnectionList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Connection>(ar.AsEnumerable());
            }
            if(CriteriaSection)
            {
                DateTime _arrivtime = DayOfArrival;
                DateTime _deptime = DayOfDeparture;
                int _arrivhour = HourOfArrival;
                int _arrivmin = MinuteOfArrival;
                int _dephour = HourOfDeparture;
                int _depmin = MinuteOfDeparture;

                using (var db = new FlyAppContext())
                {
                    var connections = from b in db.Connections
                                      where b.DeparturePlace == StartPlace && b.ArrivalPlace == DestinationPlace && b.DepartureTime.Day==DayOfDeparture.Day && b.DepartureTime.Month==DayOfDeparture.Month && b.DepartureTime.Year==DayOfDeparture.Year && Math.Abs(b.DepartureTime.Hour - HourOfDeparture) <=2 && b.IsDeleted==false
                                      select b;


                    if (!connections.Any())
                    {
                        MessageBox.Show("Connection not found.");
                    }
                    else
                    {
                        List<FlyApp.Connection> FoundedConnectionsList = connections.ToList();
                        //MessageBox.Show(FoundedConnectionsList.First().ArrivalPlace.ToString());
                        ConnectionList = new System.Collections.ObjectModel.ObservableCollection<FlyApp.Connection>(connections.AsEnumerable());
                    }
                }

            }
            
        }
    }
}
