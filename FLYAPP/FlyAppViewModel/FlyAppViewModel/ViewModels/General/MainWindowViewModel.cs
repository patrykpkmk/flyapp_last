﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace FlyAppView
{
   public class MainWindowViewModel:PropertyChangedBase
    {
        private CustomerSectionViewModel customerView;
        public CustomerSectionViewModel CustomerView
        {
            get { return customerView; }
            set {
                customerView = value;
                NotifyOfPropertyChange(() => CustomerView);
            }
        }


        private JournalViewModel _journalView;
        public JournalViewModel JournalView
        {
            get { return _journalView; }
            set
            {
                _journalView = value;
                NotifyOfPropertyChange(() => JournalView);
            }
        }

        private AdminSectionViewModel admin;
        public AdminSectionViewModel AdminView
        {
            get { return admin; }
            set
            {
                admin = value;
                NotifyOfPropertyChange(() => AdminView);
            }
        }


        public MainWindowViewModel()
        {
            CustomerView = new CustomerSectionViewModel();
            JournalView = new JournalViewModel();
            AdminView = new AdminSectionViewModel();
        }
    }
}
