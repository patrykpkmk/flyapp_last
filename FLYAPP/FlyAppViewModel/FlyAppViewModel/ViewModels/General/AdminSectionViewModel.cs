﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace FlyAppView
{
    public class AdminSectionViewModel: PropertyChangedBase
    {
        //string nazwa;
        //public String Nazwa
        //{
        //    get { return nazwa; }
        //    set { nazwa = value;
        //        NotifyOfPropertyChange(() => Nazwa);
        //    }
        //}
        private AdminSearchSectionViewModel adminSearchView;
        public AdminSearchSectionViewModel AdminSearchView
        {
            get { return adminSearchView; }
            set
            {
                adminSearchView= value;
                NotifyOfPropertyChange(() => AdminSearchView);
            }
        }

        private AdminInsertSectionViewModel adminInsertView;
        public AdminInsertSectionViewModel AdminInsertView
        {
            get { return adminInsertView; }
            set
            {
                adminInsertView = value;
                NotifyOfPropertyChange(() => AdminInsertView);
            }
        }
        public AdminSectionViewModel()
        {
            AdminSearchView = new AdminSearchSectionViewModel();
            AdminInsertView = new AdminInsertSectionViewModel();
        }
    }
}
