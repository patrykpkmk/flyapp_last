﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Windows.Forms;
using FlyAppViewModel;
using FlyApp;

namespace FlyAppView
{
   public class CustomerSectionViewModel: FlyAppViewModel.Connection
    {
        #region fields
    
        private int numberOfSeats;
        private bool studentStatus;
        private bool businessClass;
        private bool economicClass;
        private string startPlace;
        private string destinationPlace;
        #endregion
    
        #region properties


        public int NumberOfSeats
        {
            get { return numberOfSeats; }
            set
            {
                numberOfSeats = value;
                NotifyOfPropertyChange(() => NumberOfSeats);
            }
        }
        public bool StudentStatus
        {
            get { return studentStatus; }
             set
            {
                studentStatus = value;
                NotifyOfPropertyChange(() => StudentStatus);
            }
        }


        public bool BusinessClass
        {
            get { return businessClass; }
            set
            {
                businessClass = value;
                NotifyOfPropertyChange(() => BusinessClass);
            }
        }

        public bool EconomicClass
        {
            get { return economicClass; }
            set
            {
                economicClass = value;
                NotifyOfPropertyChange(() => EconomicClass);
            }
        }

        #endregion

        public CustomerSectionViewModel()
        {

        }
        public void SearchForFlight()
        {
            //int basePrice = FindBasePriceForThisTypeOfTicket();
            //List<FlyApp.Connection> ResultOfInitialSearch = FindConnections();
            //if (ResultOfInitialSearch == null) return;
            //FlyApp.Connection optimalConnection = FindOptimalConnection(ResultOfInitialSearch, basePrice);

            //MessageBox.Show(basePrice.ToString());
            FindTicket();
        }

        public int FindBasePriceForThisTypeOfTicket()
        {
            bool IsGrouped = NumberOfSeats > 4 ? true : false;
            TicketClass tclass = BusinessClass == true ? TicketClass.Business : TicketClass.Economic;
            int PriceToReturn = 0;

            using (var db = new FlyAppContext())
            {
                var _ticketTypes = from b in db.TicketTypes
                                   where  StudentStatus== b.IsStudent && tclass == b.Class && IsGrouped == b.IsGroup
                                   select b.Price;

                PriceToReturn = (int)_ticketTypes.First();
            }

            return PriceToReturn;
        }

        public List<FlyApp.Connection> FindConnections()
        {
            using (var db = new FlyAppContext())
            {
                var connections = from b in db.Connections
                                  where b.DeparturePlace == StartPlace && b.ArrivalPlace == DestinationPlace && b.DepartureTime.Day == DayOfDeparture.Day && b.DepartureTime.Month == DayOfDeparture.Month && b.DepartureTime.Year ==DayOfDeparture.Year && Math.Abs(HourOfDeparture- b.DepartureTime.Hour) <= 2 && b.IsDeleted==false
                                  select b;


                if (!connections.Any())
                {
                    MessageBox.Show("Connection not found.");
                    return null;
                }
                else
                {
                    List<FlyApp.Connection> FoundedConnectionsList = connections.ToList();

                    return FoundedConnectionsList;

                }
            }

        }

        public void FindTicket()
        {
            int BasePrice = FindBasePriceForThisTypeOfTicket();
            List<FlyApp.Connection> ResultOfInitialSearch = FindConnections();
            FlyApp.Connection FoundedBestConnection = FindOptimalConnection(ResultOfInitialSearch, BasePrice);
            double TotalTicketPrice = BasePrice * 0.1 * FoundedBestConnection.JourneyDistance;
            MessageBox.Show(String.Format("Best Ticket fro you: FROM {0} TO {1} FOR ONLY  {2}", FoundedBestConnection.DeparturePlace, FoundedBestConnection.ArrivalPlace, TotalTicketPrice));

        }

        public FlyApp.Connection FindOptimalConnection(List<FlyApp.Connection> toCheck, int BasePrice)
        {
            if (toCheck.Count == 1)
                return toCheck.First();

            FlyApp.Connection bestOne = toCheck.First();
            TimeSpan Span = bestOne.ArrivalTime.Subtract(bestOne.DepartureTime);
            double minCharge = (Span.Hours * 60 + Span.Minutes) + bestOne.JourneyDistance * BasePrice;
            foreach (FlyApp.Connection c in toCheck)
            {
                TimeSpan Span2 = c.ArrivalTime.Subtract(c.DepartureTime);
                double charge = (Span2.Hours * 60 + Span2.Minutes) + c.JourneyDistance * BasePrice;
                if (charge < minCharge)
                {
                    bestOne = c;
                    minCharge = charge;
                }
            }
            return bestOne;
        }


    }
}
