﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using System.Windows.Forms;
using FlyAppViewModel;
namespace FlyAppView
{
   public  class AdminInsertConnectionViewModel:Connection
    {
        
        private int distanceOfJourney;
        public int DistanceOfJourney
        {
            get { return distanceOfJourney; }
            set
            {
                distanceOfJourney = value;
                NotifyOfPropertyChange(() => DistanceOfJourney);
            }
        }

      

        public AdminInsertConnectionViewModel() { }
        public void AdminInsertConnection()
        {
            MessageBox.Show(HourOfArrival.ToString(), MinuteOfDeparture.ToString());
        }
    }
}
