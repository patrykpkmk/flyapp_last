﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
namespace FlyAppView
{
   public  class AdminInsertSectionViewModel:PropertyChangedBase
    {
        private AdminInsertConnectionViewModel adminInsertConnection;
        public AdminInsertConnectionViewModel AdminInsertConnectionView
        {
            get { return adminInsertConnection; }
            set
            {
                adminInsertConnection = value;
                NotifyOfPropertyChange(() => AdminInsertConnectionView);
            }
        }


        private AdminInsertAircraftViewModel adminInsertAircraft;
        public AdminInsertAircraftViewModel AdminInsertAircraftView
        {
            get { return adminInsertAircraft; }
            set
            {
                adminInsertAircraft = value;
                NotifyOfPropertyChange(() => AdminInsertAircraftView);
            }
        }


        private AdminInsertTicketTypeViewModel adminInsertTicketType;
        public AdminInsertTicketTypeViewModel AdminInsertTicketTypeView
        {
            get { return adminInsertTicketType; }
            set
            {
                adminInsertTicketType = value;
                NotifyOfPropertyChange(() => AdminInsertTicketTypeView);
            }
        }

        private AdminInsertPilotViewModel adminInsertPilot;
        public AdminInsertPilotViewModel AdminInsertPilotView
        {
            get { return adminInsertPilot; }
            set
            {
                adminInsertPilot = value;
                NotifyOfPropertyChange(() => AdminInsertPilotView);
            }
        }
        public AdminInsertSectionViewModel()
            {
            AdminInsertConnectionView = new AdminInsertConnectionViewModel();
            AdminInsertAircraftView = new AdminInsertAircraftViewModel();
            AdminInsertTicketTypeView = new AdminInsertTicketTypeViewModel();
            AdminInsertPilotView = new AdminInsertPilotViewModel();
            }
    }
}
