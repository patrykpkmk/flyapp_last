﻿#pragma checksum "..\..\..\SearchViews\AdminSearchAircraftView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "B785B2965C6E34229C547496470D411B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FlyApp;
using FlyAppView;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FlyAppView {
    
    
    /// <summary>
    /// AdminSearchAircraftView
    /// </summary>
    public partial class AdminSearchAircraftView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 48 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton IdSection;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton CriteriaSection;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ID;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Model;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NumberOfSeats;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Skills;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AdminSearchForAircraft;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FlyAppView;component/searchviews/adminsearchaircraftview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\SearchViews\AdminSearchAircraftView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.IdSection = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 2:
            this.CriteriaSection = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 3:
            this.ID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.Model = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.NumberOfSeats = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.Skills = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.AdminSearchForAircraft = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

