﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlyAppView
{
    /// <summary>
    /// Interaction logic for CustomerSectionView.xaml
    /// </summary>
    public partial class CustomerSectionView : UserControl
    {
        IEnumerable<int> Hours = Enumerable.Range(1, 24);
        IEnumerable<int> Minutes = Enumerable.Range(0, 4).Select(x => 15 * x);
        public CustomerSectionView()
        {
            InitializeComponent();
            HourOfDeparture.ItemsSource = Hours;
            MinuteOfDeparture.ItemsSource = Minutes;
        }
    }
}
