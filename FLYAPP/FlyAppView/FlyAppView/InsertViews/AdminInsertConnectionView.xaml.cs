﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlyAppView
{
    /// <summary>
    /// Interaction logic for AdminInsertConnectionView.xaml
    /// </summary>
    
    public partial class AdminInsertConnectionView : UserControl
    {
        IEnumerable<int> Hours = Enumerable.Range(1, 24);
        IEnumerable<int> Minutes = Enumerable.Range(0, 4).Select(x => 15 * x);
       
        public AdminInsertConnectionView()
        {
            InitializeComponent();
            HourOfDeparture.ItemsSource=HourOfArrival.ItemsSource = Hours;
            MinuteOfDeparture.ItemsSource=MinuteOfArrival.ItemsSource = Minutes;
            
        }
    }
}
